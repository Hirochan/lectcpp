#include <iostream>

int main (){
    auto plus = [](int a, int b) { return a + b; };
    std::cout<<plus(1,2)<<std::endl;

    auto exp = [](int a,int b){
        int r = 1;
        for(int i=0;i<b;i++){
            r *= a;
        }
        return r;
    };
    std::cout<<exp(2,5)<<std::endl;
    return 0;
}