#include <iostream>
#include <vector>
#include <algorithm>

void display(int &x){
    std::cout<<x<<std::endl;
}

int main (){
    std::vector<int> v = {1,2,3,4,5,6};

    std::for_each(v.begin(),v.end(),[](int &a){a*=2;});
    std::for_each(v.begin(),v.end(),display);
    return 0;
}