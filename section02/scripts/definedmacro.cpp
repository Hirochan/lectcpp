#if defined __clang_version__
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <string>
#include <set>
#define COMPILER "CLANG++"
#elif defined __GNUC__
#include <bits/stdc++.h>
#define COMPILER "GCC++"
#endif

int main(int argc, char const *argv[])
{
    std::cout<<COMPILER<<std::endl;
    return 0;
}
