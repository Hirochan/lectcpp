#include <iostream>
#include <string>

class Human
{
public:
    Human(double __hight__, double __weight__, std::string __name__);
    std::string name;
    double BMI;
    void display();
    ~Human();
private:
    double hight;
    double weight;
};

Human::Human(double __hight__, double __weight__, std::string __name__){
    hight = __hight__;
    weight = __weight__;
    BMI = __weight__ / ((__hight__ / 100.0) * (__hight__ / 100.0));
    name = __name__;
}

void Human::display(){
    std::cout<<"------------------------"<<std::endl;
    std::cout<<"名前\t: "<<name<<std::endl;
    std::cout<<"身長\t: "<<hight<<"(cm)"<<std::endl;
    std::cout<<"体重\t: "<<weight<<"(kg)"<<std::endl;
    std::cout<<"BMI\t: "<<BMI<<std::endl;
    std::cout<<"------------------------"<<std::endl;
}
Human::~Human()
{

}

int main()
{
    // Human型のKittyを実体化
    Human Kitty(42.5,0.9,"Kitty White");

    // Kittyのメソッドのdisplay関数を呼び出す
    Kitty.display();
    return 0;
}
